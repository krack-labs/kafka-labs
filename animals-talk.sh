ANIMALS="
Dog:Rex:woof
Dog:Rex:wif
Dog:Médor:waf
Dog:Médor:awoo
Dog:Médor:wof
Cat:Minouche:miaw
Cat:Minouche:mieow
Cat:Minouche:mew
Cat:Bambou:mreow
Cat:Bambou:mreoow
Cat:Bambou:mreooow
"

function getSpecies() { echo "$1" | cut -d: -f1 ; }
function getName() { echo "$1" | cut -d: -f2 ; }
function getSound() { echo "$1" | cut -d: -f3 ; }

function getMessage() { echo "$(getName $1) says $(getSound $1) ($(getSpecies $1),$COUNT)" ; }
function getRandomKey() { echo $RANDOM ; }

COUNT=0
TOPIC=demo-2

while true ; do 
    for ANIMAL in $ANIMALS ; do

        MESSAGE=$(getMessage $ANIMAL)
        
        KEY=$(getRandomKey)
        # KEY="1234"
        # KEY=$(getSpecies $ANIMAL)
        # KEY=$(getName $ANIMAL)

        echo "$KEY:$MESSAGE" | kafkacat -P -T -K : -b localhost:9094 -t $TOPIC
        sleep 1
    done
    let COUNT++
done
