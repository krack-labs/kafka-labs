# Kafka Labs

Un cluster Kafka composé de 1 broker est automatiquement démarré dans cet environnement GitPod.

Le broker est joignable à l'adresse `kafka:9094`.

Tester la disponibilité du cluster :

```sh
kafkacat -b localhost:9094 -L
```

Les topics suivants ont déjà été créés :

- demo-1
- demo-2
- demo-12

Découvrez le fonctionnement de Kafka avec l'outil `kafkacat` en échangeant des messages via des topics.

## TP 1 : Pub/Sub 💌

1. Produisez (envoyer) quelques messages dans le topic `demo-1` :

    ```sh
    # Ouvrir le prompt kafkacat puis envoyer des messages avec <enter>:
    kafkacat -b localhost:9094 -t demo-1 -P
    ```

2. Consommez les messages dans un terminal différent :

    ```sh
    kafkacat -b localhost:9094 -q -t demo-1 -C
    ```

3. Produisez de nouveaux messages.

**Qu'observez-vous sur l'échange entre producteur et consommateur ?**

<details>
<summary>🔎 Solution</summary>

> Le producteur et le consommateur peuvent échanger des messages de manière asynchrone, sans obligation de présence.
> Les messages sont traités au fil de l'eau lorsque le consommateur est présent.  

</details>

## TP 2 : Groupes 💙🤍💖

### Ex 2.1

Arrêtez et relancez le consommateur. 

**Qu'observez-vous concernant les messages lus ?**

<details>
<summary>🔎 Solution</summary>

> Le consommateur lit à nouveau tous les messages. Par défaut, un consommateur ne garde pas en mémoire sa progression.

</details>

### Ex 2.2

1. Recréez le consommateur en configurant un groupe `group1` :

    ```sh
    kafkacat -b localhost:9094 -q -G group1 demo-1
    ```

2. Envoyez des messages et redémarrez le consommateur plusieurs fois : 

> Les messages déjà lus sont désormais gardés en mémoire, ils ne sont pas consommés à nouveau.

3. Créez les groupes 2 et 3 dans d'autres terminaux :

    ```sh
    # Groupe 2
    kafkacat -b localhost:9094 -q -G group2 demo-1
    ```

    ```sh
    # Groupe 3
    kafkacat -b localhost:9094 -q -G group3 demo-1
    ```

4. Produisez de nouveaux messages.

**Qu'observez-vous concernant les messages lus ?**

<details>
<summary>🔎 Solution</summary>

> Les messages sont consommés indépendamment par les différents groupes.  

</details>

> ℹ Noter qu'il est possible de rejouer la lecture de messages déjà consommés par un groupe. (`-o` `beginning`, `end`, X derniers, n#).

## TP 3 : Partitions et parallélisme 🎿

### Ex 3.1

1. Arrêtez les consommateurs 2 et 3.
2. Ajoutez un second consommateur au groupe 1 en parallèle du consommateur existant :

    ```sh
    kafkacat -b localhost:9094 -q -G group1 demo-1
    ```
3. Produisez des messages.

**Qu'observez-vous concernant les messages lus par les consommateurs ?**

<details>
<summary>🔎 Solution</summary>

> Les messages sont tous absorbés par le premier consommateur. En effet, le topic `demo-1` ne possède qu'une seule partition, et une partition est assignée à un seul consommateur.

</details>

### Ex 3.2

1. Recréez le producteur et les 2 consommateurs en ciblant maintenant le topic `demo-2` :

    ```sh
    # Producteur
    kafkacat -b localhost:9094 -q -t demo-2 -P
    ```

    ```sh
    # Consommateurs
    kafkacat -b localhost:9094 -q -G group1 demo-2
    ```
2. Envoyez plusieurs messages

**Qu'observez-vous concernant les messages lus par les consommateurs ?**

<details>
<summary>🔎 Solution</summary>

> Les messages sont répartis entre les deux consommateurs grâce à la présence de deux partitions dans le topic.

</details>

### Ex 3.3 : Clé de partitionnement

1. Lancez le script `animals-talk.sh`. Ce script produit des messages dans le topic `demo-2` sans préciser de **clé de partitionnement**.

```sh
./animals-talk.sh
```

2. Consommez les messages de `demo-2` avec **deux** consommateurs d'un même groupe :

```sh
# Créez deux consommateurs (la commande affiche la clé de partitionnement)
kafkacat -b localhost:9094 -q -u -K ' : ' -G group1 demo-2
```

3. Modifiez le script pour utiliser différentes clés de partitionnement. Par défaut, il utilise une clé de partitionnement aléatoire.

    - Qu'observez vous avec une clé de partionnement **aléatoire** ?
    - Qu'observez vous avec une clé de partitionnement **fixe** ?
    - Qu'observez vous avec le **type d'animal** (`species`) comme clé de partitionnement ?
    - Qu'observez vous avec le **nom de l'animal** comme clé de partitionnement ?

<details>
<summary>🔎 Solution</summary>

> Le choix de la clé détermine la distribution des messages dans les partitions et donc le groupement par consommateur. La clé est essentielle pour garantir la cohérence et l'ordre des traitements.

</details>

## TP 4 : Scalabilité 🐰🐢

Continuez de produire des messages avec le script.

1. Gardez un seul consommateur dans le `group1` comme témoin.

2. Créez un groupe de consommation avec la commande ci-dessous, simulant un traitement lourd/lent de **2,5 secondes** sur chaque message.

```sh
TOPIC=demo-2
kafkacat -b localhost:9094 -G group2 $TOPIC -X fetch.min.bytes=100 -X fetch.message.max.bytes=1 -X fetch.wait.max.ms=2500
```

Les messages sont produits au rythme de 1 msg/s. Comment faire pour que le groupe 2 ne prenne pas de retard ?

<details>
<summary>🔎 Solution</summary>

> Il serait nécessaire de créer plusieurs consommateurs dans `group2` pour suivre la charge de travail.  
> Cependant, il faut que le topic ait au moins autant de partitions que de membres du groupe.  
> En utilisant le topic `demo-12` qui comporte 12 partitions, on peut répartir la charge sur 3 consommateurs (jusqu'à 12).
> **Conclusion** : Le nombre de partitions d'un topic est un facteur clé de la capacité de mise à l'échelle d'un traitement.

</details>

## Quiz

👀 https://forms.office.com/e/k49P1Sg75H